import warnings
import os
import sys
# import neuralnetwork
import ga

warnings.simplefilter(
    action='ignore', category=FutureWarning
)  # Gets rid of weird integer error
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Gets rid another tensorflow thing

if __name__ == '__main__':
    # Get the global values for generation and best fitness
    generation, best_fitness, best_generation, best_accuracy = 0, 0, -1, 0

    # If statement to check if the user
    # enters all the inputs beside the python command
    if len(sys.argv) == 5:
        # Get input for N, max generations,
        #   probability of crossover(Pc), probability of mutation(Pm)
        # Get Pc and Pm as floats multiple them
        #   by 100 and then convert them into a float
        N = int(sys.argv[1])
        max_generations = int(sys.argv[2])
        Pc = float(sys.argv[3]) * 100
        Pc = int(Pc)
        Pm = float(sys.argv[4]) * 100
        Pm = int(Pm)

        # Get initial population of neural networks
        population, logits = ga.initial_population(N)
    else:
        # Get input for N, max generations,
        #   probability of crossover(Pc), probability of mutation(Pm)
        # Get Pc and Pm as floats multiple them
        #   by 100 and then convert them into a float
        N = int(input("Enter in N value(population size): "))
        max_generations = int(
            input("Enter in the max number of generations: ")
        )
        Pc = float(input("Enter in the crossover probability: ")) * 100
        Pc = int(Pc)
        Pm = float(input("Enter in the mutation probability: ")) * 100
        Pm = int(Pm)

        # Get initial population of neural networks
        population, logits = ga.initial_population(N)

    # Loop till we hit the max number of generation
    while generation < max_generations:
        # Print out wait generation we are on
        print('\nGeneration:', generation)

        # Get a new population of neural networks
        new_pop = ga.new_population(N, population, Pc, Pm, logits)

        # For loop to print the data of the
        #   new population then copy it to the original population
        for i in range(N):
            # Print out some of the information about the neural networks
            print('Accuracy: {}\tLR: {}\tEpochs: {}'.format(
                new_pop[i].accuracy,
                new_pop[i].learning_rate,
                new_pop[i].training_epochs),
                end=" "
            )
            print('Num of layers: {}\tNodes in layer: {}\tFitness: {}'.format(
                new_pop[i].num_layers,
                new_pop[i].nodes_in_layer,
                new_pop[i].fitness)
            )

            # If statement for adjusting the best fitness score
            if new_pop[i].accuracy == best_accuracy:
                if new_pop[i].fitness < best_fitness:
                    best_fitness = new_pop[i].fitness

            # If statement for adjusting the best accuracy and fitness score
            if new_pop[i].accuracy > best_accuracy:
                best_accuracy = new_pop[i].accuracy
                best_fitness = new_pop[i].fitness

            # Copy the new population to the old population
            population[i].copy(
                new_pop[i].training_epochs, new_pop[i].num_layers,
                new_pop[i].nodes_in_layer, new_pop[i].weights,
                new_pop[i].biases, new_pop[i].loss, new_pop[i].optimizer,
                new_pop[i].trainingOp, new_pop[i].accuracy, new_pop[i].fitness
            )

        print()  # Print statement to format output

        # If the best accuracy is
        #   100 we break because that is the max accuracy we can get
        if best_accuracy == 100:
            best_generation = generation
            break

        generation += 1  # Increment generation

    if best_generation == -1:
        print('Best Fitness:', best_fitness)
    else:
        print('Best Accuracy: {} Best Fitness: {} Generation Found: {}'.format(
            best_accuracy, best_fitness, best_generation)
        )
