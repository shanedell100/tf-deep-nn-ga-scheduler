import random
import neuralnetwork
import tensorflow.compat.v1 as tf


# Function that calculates accuracy
def calculate_accuracy(list_of_neural_nets, logits, j, called_from, index):
    init = tf.global_variables_initializer()

    with tf.Session() as training_session:
        training_session.run(init)

        for epoch in range(list_of_neural_nets[index].training_epochs):
            total_batch = 1

            for i in range(total_batch):
                batch_x, batch_y = neuralnetwork.trainX, neuralnetwork.trainY

                _, c = training_session.run([
                    list_of_neural_nets[i].trainingOp,
                    list_of_neural_nets[i].loss
                ], feed_dict={
                    neuralnetwork.X: batch_x,
                    neuralnetwork.Y: batch_y
                })

        # Test
        prediction = tf.nn.softmax(logits)
        correct_prediction = tf.equal(
            tf.argmax(prediction, 1),
            tf.argmax(neuralnetwork.Y, 1)
        )

        # Calculate Accuracy
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        return accuracy.eval(
            {
                neuralnetwork.X: neuralnetwork.testX,
                neuralnetwork.Y: neuralnetwork.testY
            }
        )


# Function that compares three random
#   neural networks and the winner is the one with the best accuracy
def tournament_selection(population, N):
    winner = neuralnetwork.NeuralNetwork()
    loop = False

    random_index1, random_index2, random_index3 = 0, 0, 0

    while not loop:
        random_index1 = random.randint(0, N - 1)
        random_index2 = random.randint(0, N - 1)
        random_index3 = random.randint(0, N - 1)

        if (random_index1 != random_index2 and random_index1 != random_index3
                and random_index2 != random_index3):
            loop = True

    # Find which index has the best accuracy
    #   and copy its information into the winner neural network
    if (population[random_index1].accuracy
            > population[random_index2].accuracy
            and population[random_index1].accuracy
            > population[random_index3].accuracy
            and population[random_index1].training_epochs
            > population[random_index2].training_epochs
            and population[random_index1].training_epochs
            > population[random_index3].training_epochs):
        winner.copy(population[random_index1].training_epochs,
                    population[random_index1].num_layers,
                    population[random_index1].nodes_in_layer,
                    population[random_index1].weights,
                    population[random_index1].biases,
                    population[random_index1].loss,
                    population[random_index1].optimizer,
                    population[random_index1].trainingOp,
                    population[random_index1].accuracy,
                    population[random_index1].fitness)
    elif (population[random_index2].accuracy
            > population[random_index1].accuracy
            and population[random_index2].accuracy
            > population[random_index3].accuracy
            and population[random_index2].training_epochs
            > population[random_index1].training_epochs
            and population[random_index2].training_epochs
            > population[random_index3].training_epochs):
        winner.copy(population[random_index2].training_epochs,
                    population[random_index2].num_layers,
                    population[random_index2].nodes_in_layer,
                    population[random_index2].weights,
                    population[random_index2].biases,
                    population[random_index2].loss,
                    population[random_index2].optimizer,
                    population[random_index2].trainingOp,
                    population[random_index2].accuracy,
                    population[random_index2].fitness)
    else:
        winner.copy(population[random_index3].training_epochs,
                    population[random_index3].num_layers,
                    population[random_index3].nodes_in_layer,
                    population[random_index3].weights,
                    population[random_index3].biases,
                    population[random_index3].loss,
                    population[random_index3].optimizer,
                    population[random_index3].trainingOp,
                    population[random_index3].accuracy,
                    population[random_index3].fitness)

    # Return the neural network that won
    return winner


# When crossover is done the function will generate a random number and
# that specific attribute of NeuralNetwork class will be swapped between parent
def crossover(parent_1, parent_2, Pc, logits):
    do_crossover = (random.randint(1, 100))

    if do_crossover <= Pc:
        # don't include trainingOp because it changes
        #   everytime the loss or optimizer function is switched
        crossover_point = random.randint(0, 6)

        if crossover_point == 0:  # Switch number of training epochs
            temp = parent_1.training_epochs
            parent_1.training_epochs = parent_2.training_epochs
            parent_2.training_epochs = temp
        elif crossover_point == 1:  # Switch the number of layers
            temp = parent_1.num_layers
            parent_1.num_layers = parent_2.num_layers
            parent_2.num_layers = temp
        elif crossover_point == 2:  # Switch the number of nodes in a layer
            temp = parent_1.nodes_in_layer
            parent_1.nodes_in_layer = parent_2.nodes_in_layer
            parent_2.nodes_in_layer = temp
        elif crossover_point == 3:  # Switch the weights
            temp = parent_1.weights
            parent_1.weights = parent_2.weights
            parent_2.weights = temp
        elif crossover_point == 4:  # Switch the biases
            temp = parent_1.biases
            parent_1.biases = parent_2.biases
            parent_2.biases = temp
        elif crossover_point == 5:  # Switch loss functions
            temp = parent_1.loss
            parent_1.loss = parent_2.loss
            parent_2.loss = temp
            parent_1.trainingOp = parent_1.optimizer.minimize(parent_1.loss)
            parent_2.trainingOp = parent_2.optimizer.minimize(parent_2.loss)
        elif crossover_point == 6:  # Switch optimizer functions
            temp = parent_1.optimizer
            parent_1.optimizer = parent_2.optimizer
            parent_2.optimizer = temp
            parent_1.trainingOp = parent_1.optimizer.minimize(parent_1.loss)
            parent_2.trainingOp = parent_2.optimizer.minimize(parent_2.loss)


# Function to do mutation when it is needed
def mutation(parent, Pm, logits):
    do_mutation = random.randint(1, 100)

    if do_mutation <= Pm:
        # 5 instead of 6 b/c I mutate the
        #   trainingOp whenever I mutate the loss or optimizer
        mutation_point = random.randint(0, 5)

        # Always change the amount of epics and
        #   nodes in layer when mutation happens
        parent.training_epochs = random.randint(10, 1000)
        parent.nodes_in_layer = random.randint(100, 1500)

        # Everytime the num of layers or the number of nodes
        #   in those layers are changed I also redo the weights and the biases
        if mutation_point == 0:  # change number of layers
            parent.num_layers = random.randint(3, 8)
            for i in range(parent.num_layers):
                if i == 0:
                    parent.weights.update({
                        "hidden_layer" + str(i + 1): tf.Variable(
                            tf.random.normal([
                                neuralnetwork.number_inputs,
                                parent.nodes_in_layer
                            ])
                        )
                    })
                    parent.biases.update({
                        "bias_layer" + str(i + 1): tf.Variable(
                            tf.random.normal([parent.nodes_in_layer])
                        )
                    })
                elif i == parent.num_layers - 1:
                    parent.weights.update({
                        "output_layer": tf.Variable(
                            tf.random.normal([
                                parent.nodes_in_layer,
                                neuralnetwork.number_outputs
                            ])
                        )
                    })
                    parent.biases.update({
                        "output_layer": tf.Variable(
                            tf.random.normal([
                                neuralnetwork.number_outputs])
                        )
                    })
                else:
                    parent.weights.update({
                        "hidden_layer" + str(i + 1): tf.Variable(
                            tf.random.normal([
                                parent.nodes_in_layer,
                                parent.nodes_in_layer
                            ])
                        )
                    })
                    parent.biases.update({
                        "bias_layer" + str(i + 1): tf.Variable(
                            tf.random.normal([
                                parent.nodes_in_layer
                            ])
                        )
                    })
        elif mutation_point == 1:
            # Do nothing
            parent.nodes_in_layer = parent.nodes_in_layer
        elif mutation_point == 2:  # Change all the weights
            for i in range(parent.num_layers):
                if i == 0:
                    parent.weights["hidden_layer" + str(i + 1)] = tf.Variable(
                        tf.random.normal([
                            neuralnetwork.number_inputs,
                            parent.nodes_in_layer
                        ])
                    )
                elif i == parent.num_layers - 1:
                    parent.weights["output_layer"] = tf.Variable(
                        tf.random.normal([
                            parent.nodes_in_layer,
                            neuralnetwork.number_outputs
                        ])
                    )
                else:
                    parent.weights["hidden_layer" + str(i + 1)] = tf.Variable(
                        tf.random.normal([
                            parent.nodes_in_layer,
                            parent.nodes_in_layer
                        ])
                    )
        elif mutation_point == 3:  # Change all the biases
            for i in range(parent.num_layers):
                if i == 0:
                    parent.biases["bias_layer" + str(i + 1)] = tf.Variable(
                        tf.random.normal([
                            parent.nodes_in_layer
                        ])
                    )
                elif i == parent.num_layers - 1:
                    parent.biases["output_layer"] = tf.Variable(
                        tf.random.normal([
                            neuralnetwork.number_outputs
                        ])
                    )
                else:
                    parent.biases["bias_layer" + str(i + 1)] = tf.Variable(
                        tf.random.normal([
                            parent.nodes_in_layer
                        ])
                    )
        elif mutation_point == 4:  # Change the loss function
            # Randomly choose a loss function
            loss_index = random.randrange(0, len(neuralnetwork.losses))

            if (neuralnetwork.losses[loss_index] ==
                    'sigmoid_cross_entropy_with_logits'):
                parent.loss = tf.reduce_mean(
                    tf.nn.sigmoid_cross_entropy_with_logits(
                        logits=logits, labels=neuralnetwork.Y
                    )
                )
            elif (neuralnetwork.losses[loss_index] ==
                    'softmax_cross_entropy_with_logits_v2'):
                parent.loss = tf.reduce_mean(
                    tf.nn.softmax_cross_entropy_with_logits_v2(
                        logits=logits, labels=neuralnetwork.Y
                    )
                )

            # Update training operator
            parent.trainingOp = parent.optimizer.minimize(parent.loss)
        elif mutation_point == 5:  # Change optimizer function
            # Randomly choose an optimizer
            optimizer_index = random.randrange(
                0, len(neuralnetwork.optimizers)
            )

            if (neuralnetwork.optimizers[optimizer_index] ==
                    "AdadeltaOptimizer"):
                parent.optimizer = tf.train.AdadeltaOptimizer(
                    learning_rate=parent.learning_rate
                )
            elif (neuralnetwork.optimizers[optimizer_index] ==
                    "AdagradOptimizer"):
                parent.optimizer = tf.train.AdagradOptimizer(
                    learning_rate=parent.learning_rate
                )
            elif (neuralnetwork.optimizers[optimizer_index] ==
                    "AdamOptimizer"):
                parent.optimizer = tf.train.AdamOptimizer(
                    learning_rate=parent.learning_rate
                )
            elif (neuralnetwork.optimizers[optimizer_index] ==
                    "FtrlOptimizer"):
                parent.optimizer = tf.train.FtrlOptimizer(
                    learning_rate=parent.learning_rate
                )
            elif (neuralnetwork.optimizers[optimizer_index] ==
                    "GradientDescentOptimizer"):
                parent.optimizer = tf.train.GradientDescentOptimizer(
                    learning_rate=parent.learning_rate
                )

            # Update training operator
            parent.trainingOp = parent.optimizer.minimize(parent.loss)

    # Return either the new changed neural network
    #    or just return the one we were sent
    return parent


# Function to create initial population of neural networks
def initial_population(N):
    list_of_neural_nets = []
    counter = 1
    list_of_accuracies = []

    for i in range(N):
        nn = neuralnetwork.NeuralNetwork()
        logits = neuralnetwork.Forward_Propagate_Model(neuralnetwork.X, nn)

        # Randomly choose a loss function
        loss_index = random.randrange(0, len(neuralnetwork.losses))

        if (neuralnetwork.losses[loss_index] ==
                'sigmoid_cross_entropy_with_logits'):
            nn.loss = tf.reduce_mean(
                tf.nn.sigmoid_cross_entropy_with_logits(
                    logits=logits, labels=neuralnetwork.Y
                )
            )
        elif (neuralnetwork.losses[loss_index] ==
                'softmax_cross_entropy_with_logits_v2'):
            nn.loss = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits_v2(
                    logits=logits, labels=neuralnetwork.Y
                )
            )

        # Randomly choose an optimizer
        optimizer_index = random.randrange(0, len(neuralnetwork.optimizers))

        if neuralnetwork.optimizers[optimizer_index] == "AdadeltaOptimizer":
            nn.optimizer = tf.train.AdadeltaOptimizer(
                learning_rate=nn.learning_rate
            )
        elif neuralnetwork.optimizers[optimizer_index] == "AdagradOptimizer":
            nn.optimizer = tf.train.AdagradOptimizer(
                learning_rate=nn.learning_rate
            )
        elif neuralnetwork.optimizers[optimizer_index] == "AdamOptimizer":
            nn.optimizer = tf.train.AdamOptimizer(
                learning_rate=nn.learning_rate
            )
        elif neuralnetwork.optimizers[optimizer_index] == "FtrlOptimizer":
            nn.optimizer = tf.train.FtrlOptimizer(
                learning_rate=nn.learning_rate
            )
        elif (neuralnetwork.optimizers[optimizer_index] ==
                "GradientDescentOptimizer"):
            nn.optimizer = tf.train.GradientDescentOptimizer(
                learning_rate=nn.learning_rate
            )

        nn.trainingOp = nn.optimizer.minimize(nn.loss)
        list_of_neural_nets.append(nn)

        list_of_accuracies.append(
            calculate_accuracy(
                list_of_neural_nets, logits, counter, 'ip', i
            )
        )
        counter += 1

    for i in range(N):
        # Convert accuracy to an int so it is easier to use
        list_of_neural_nets[i].accuracy = list_of_accuracies[i] * 100
        list_of_neural_nets[i].accuracy = int(list_of_neural_nets[i].accuracy)
        list_of_neural_nets[i].fitness = (
            list_of_neural_nets[i].accuracy +
            list_of_neural_nets[i].training_epochs +
            list_of_neural_nets[i].num_layers +
            list_of_neural_nets[i].nodes_in_layer
        )

        # This is just to keep fitness scores a little lower
        if (list_of_neural_nets[i].fitness != 0 and
                list_of_neural_nets[i].fitness >= 10000):
            list_of_neural_nets[i].fitness = round(
                list_of_neural_nets[i].fitness / 10000
            )

    return list_of_neural_nets, logits


# Function to create a new population of neural networks
def new_population(N, population, Pc, Pm, logits):
    new_pop_of_nn = []
    count = 0
    list_of_accuracies = []
    counter = 1

    for i in range(0, N, 2):
        parent_A = neuralnetwork.NeuralNetwork()
        parent_B = neuralnetwork.NeuralNetwork()

        while parent_A.accuracy == parent_B.accuracy:
            parent_A = tournament_selection(population, N)
            parent_B = tournament_selection(population, N)
            count += 1

            if count >= 15:
                break

        # Call crossover function to see if crossover will be performed
        crossover(parent_A, parent_B, Pc, logits)

        # Call mutation function with parentA
        parent_A = mutation(parent_A, Pm, logits)

        # Call mutation function with parentB
        parent_B = mutation(parent_B, Pm, logits)

        # Add both parents to the new population/list
        new_pop_of_nn.append(parent_A)
        new_pop_of_nn.append(parent_B)

    for i in range(N):
        list_of_accuracies.append(
            calculate_accuracy(new_pop_of_nn, logits, counter, 'np', i)
        )
        counter += 1

    for i in range(N):
        # Convert accuracy to an int so it is easier to use
        new_pop_of_nn[i].accuracy = list_of_accuracies[i] * 100
        new_pop_of_nn[i].accuracy = int(new_pop_of_nn[i].accuracy)
        new_pop_of_nn[i].fitness = (
            new_pop_of_nn[i].accuracy * new_pop_of_nn[i].training_epochs *
            new_pop_of_nn[i].num_layers * new_pop_of_nn[i].nodes_in_layer
        )

        # This is just to keep fitness scores a little lower
        if new_pop_of_nn[i].fitness != 0 and new_pop_of_nn[i].fitness >= 10000:
            new_pop_of_nn[i].fitness = round(new_pop_of_nn[i].fitness / 10000)

    # Return the new_list once full
    return new_pop_of_nn
