FROM python:3.6

COPY . /app
WORKDIR /app

RUN python3 -m pip install tensorflow==1.15

ENTRYPOINT [ "python3", "main.py", "10", "10000", ".75", ".5" ]
