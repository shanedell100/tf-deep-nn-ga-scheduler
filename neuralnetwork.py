import csv
import tensorflow.compat.v1 as tf
import random

# Used random optimizers that were on the
#   tensorflow docs that didn't give me an error when chosen
optimizers = [
    "AdadeltaOptimizer", "AdagradOptimizer",
    "AdamOptimizer", "FtrlOptimizer", "GradientDescentOptimizer"
]

# Loss functions that uses logits and that didn't give me an error when chosen
losses = [
    'sigmoid_cross_entropy_with_logits',
    'softmax_cross_entropy_with_logits_v2'
]

# Didn't know if these should change
number_inputs = 4
number_outputs = 4


# Class for a instance of a neural network
class NeuralNetwork():
    # Initial Constructor
    def __init__(self, number_inputs=4, number_outputs=4):
        # Create random numbers for the training epochs,
        #   learning rate, number of layers, and nodes in the layers
        self.training_epochs = random.randint(10, 1000)
        self.learning_rate = round(random.uniform(0.002, 0.9), 3)
        self.num_layers = random.randint(3, 8)
        self.nodes_in_layer = random.randint(100, 1500)

        # Create Initial dictionaries for the weights and biases
        self.weights = {}
        self.biases = {}

        # For loop to create all the weights and biases
        #   needed for a single neural network
        for i in range(self.num_layers):
            if i == 0:
                self.weights.update({
                    "hidden_layer" + str(i + 1): tf.Variable(
                        tf.random.normal([
                            number_inputs, self.nodes_in_layer
                        ])
                    )
                })
                self.biases.update({
                    "bias_layer" + str(i + 1): tf.Variable(
                        tf.random.normal([self.nodes_in_layer])
                    )
                })
            elif i == self.num_layers - 1:
                self.weights.update({
                    "output_layer": tf.Variable(
                        tf.random.normal([
                            self.nodes_in_layer, number_outputs
                        ])
                    )
                })
                self.biases.update({
                    "output_layer": tf.Variable(
                        tf.random.normal([number_outputs])
                    )
                })
            else:
                self.weights.update({
                    "hidden_layer" + str(i + 1): tf.Variable(
                        tf.random.normal([
                            self.nodes_in_layer, self.nodes_in_layer
                        ])
                    )
                })
                self.biases.update({
                    "bias_layer" + str(i + 1): tf.Variable(
                        tf.random.normal([self.nodes_in_layer])
                    )
                })

        # Default accuracy and fitness is 0
        self.accuracy = 0
        self.fitness = 0

    # Copy method to transfer attributes between neural networks
    def copy(self, training_epochs, num_layers, nodes_in_layer,
             weights, biases, loss, optimizer, trainingOp, accuracy, fitness):
        self.training_epochs = training_epochs
        self.num_layers = num_layers
        self.nodes_in_layer = nodes_in_layer
        self.weights = weights
        self.biases = biases
        self.loss = loss
        self.optimizer = optimizer
        self.trainingOp = trainingOp
        self.accuracy = accuracy
        self.fitness = fitness


# Create all the training and test data
trainX = []
trainY = []
file = open('training.csv', 'r', encoding='utf-8')
filereader = csv.reader(file)
data_as_list = list(filereader)

for line in data_as_list[1:]:
    feature = []
    label = []

    for item in line[:4]:
        feature.append(float(item))

    for item in line[4:]:
        label.append(float(item))

    trainX.append(feature)
    trainY.append(label)
file.close()

testX = []
testY = []
file = open('test.csv', 'r', encoding='utf-8')
filereader = csv.reader(file)
filereader = list(filereader)

for line in filereader[1:]:
    feature = []
    label = []

    for item in line[:4]:
        feature.append(float(item))

    for item in line[4:]:
        label.append(float(item))

    testX.append(feature)
    testY.append(label)
file.close()

batch_size = len(trainY)
display_step = 1

X = tf.placeholder(name='X', dtype=tf.float32, shape=[None, 4])
Y = tf.placeholder(name='Y', dtype=tf.float32, shape=[None, 4])


# Function to create a forward propagated model
def Forward_Propagate_Model(x, nn):
    _layer1 = tf.add(
        tf.matmul(x, nn.weights['hidden_layer1']),
        nn.biases['bias_layer1']
    )

    # Variable to keep track of the previous layer
    previous_layer = _layer1

    # For loop to create all the hidden layers needed
    for i in range(2, nn.num_layers):
        new_layer = tf.add(
            tf.matmul(previous_layer, nn.weights['hidden_layer' + str(i)]),
            nn.biases['bias_layer' + str(i)]
        )
        previous_layer = new_layer

    # Create output layer, this is how the output layer always has to be made
    _output_layer = tf.add(
        tf.matmul(previous_layer, nn.weights['output_layer']),
        nn.biases['output_layer']
    )

    # Return the model
    return _output_layer
